package db

import "context"

const dbName = "specters"

// InstanceID is the type used in instance identities.
type InstanceID string

// PrivateKeyDB is an interface to sign
type PrivateKeyDB interface {
	// Sign the given bytes cryptographically.
	Sign([]byte) ([]byte, error)

	// GetPublicDB returns the public key DB paired with this identity.
	GetPublicDB() PublicKeyDB
}

// PublicKeyDB is an interface to verify
type PublicKeyDB interface {
	// Verify that 'sig' is the signed hash of 'data'
	Verify([]byte, []byte) (bool, error)
}

// DB is an interface for specters storage
type DB interface {
	// Close closes DB
	Close() error

	// CreateSpecters creates the Specters DB if it doesn't exist
	CreateSpecters(context.Context, InstanceDB) error

	// GetSpecter gets a Specter from Specters DB
	GetSpecter(string, InstanceDB, PublicKeyDB) (InstanceDB, error)

	// HasSpecters returns if at least one specter exists
	HasSpecters() (bool, error)

	// CreateSpecter creates a Specter in Specters DB
	CreateSpecter(InstanceDB, PrivateKeyDB) (InstanceID, error)
}

// InstanceDB is an interface to sign and verify instances
type InstanceDB interface {
	// GetID gets the ID of InstanceDB
	GetID() InstanceID

	// SetID sets the ID on InstanceDB
	SetID(InstanceID)

	// SignInstance the given instance cryptographically.
	SignInstance(PrivateKeyDB) error

	// VerifyInstance that 'sig' is the signed hash of instance
	VerifyInstance(PublicKeyDB) (bool, error)
}
