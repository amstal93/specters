package specters

import (
	"encoding/json"
	"fmt"
	"gitlab.com/socialspecters.io/specters/pkg/db"
	"time"
)

// Specter is an app interface for Specter identity
type Specter interface {
	PubKey
	db.InstanceDB

	// Init initializes the keys
	Init() error

	// GetPubKey gets the public key
	GetPubKey() string
}

// SpecterDB is Textile Threads DB specter identity
type SpecterDB struct {
	ID        string `json:"_id"`
	PubKey    string `json:"public_key"`
	Alias     string `json:"alias"`
	CreatedAt int64  `json:"created_at"`
	Signature string `json:"signature"`

	pubKey PubKey
}

// NewSpecterOptions defines options for creating a new Specter.
type NewSpecterOptions struct {
	Alias string
}

// NewSpecterOption specifies a new Specter option.
type NewSpecterOption func(*NewSpecterOptions)

// WithAlias provides control over alias for Specter.
func WithAlias(alias string) NewSpecterOption {
	return func(o *NewSpecterOptions) {
		o.Alias = alias
	}
}

// NewSpecterDB creates a new instance of SpecterDB
func NewSpecterDB(pk string, opts ...NewSpecterOption) (s Specter, err error) {
	args := &NewSpecterOptions{}
	for _, opt := range opts {
		opt(args)
	}

	s = &SpecterDB{
		PubKey:    pk,
		Alias:     args.Alias,
		CreatedAt: time.Now().Unix(),
	}
	err = s.Init()
	return
}

// Init initializes the keys
func (s *SpecterDB) Init() error {
	decodedKey, err := DecodeKey(s.PubKey)
	if err != nil {
		return err
	}

	err = s.UnmarshalBinary(decodedKey)
	if err != nil {
		return err
	}

	return nil
}

// GetPubKey gets the public key
func (s *SpecterDB) GetPubKey() string {
	return s.PubKey
}

// MarshalBinary converts an identity object into its protobuf serialized form.
func (s *SpecterDB) MarshalBinary() ([]byte, error) {
	if s.pubKey == nil {
		return nil, fmt.Errorf("SpecterDB instance is not initialized")
	}
	return s.pubKey.MarshalBinary()
}

// UnmarshalBinary converts a protobuf serialized identity into its
// representative object
func (s *SpecterDB) UnmarshalBinary(data []byte) error {
	pubKey, err := UnmarshalLibP2PPubKey(data)
	if err != nil {
		return err
	}
	s.pubKey = pubKey
	return nil
}

// Raw returns the raw bytes of the key (not wrapped in the
// libp2p-crypto protobuf).
//
// This function is the inverse of {Priv,Pub}KeyUnmarshaler.
func (s *SpecterDB) Raw() ([]byte, error) {
	if s.pubKey == nil {
		return nil, fmt.Errorf("SpecterDB instance is not initialized")
	}
	return s.pubKey.Raw()
}

// Verify that 'sig' is the signed hash of 'data'
func (s *SpecterDB) Verify(data []byte, sigBytes []byte) (bool, error) {
	if s.pubKey == nil {
		return false, fmt.Errorf("SpecterDB instance is not initialized")
	}
	return s.pubKey.Verify(data, sigBytes)
}

// GetID gets the ID of InstanceDB
func (s *SpecterDB) GetID() db.InstanceID {
	return db.InstanceID(s.ID)
}

// SetID sets the ID on InstanceDB
func (s *SpecterDB) SetID(id db.InstanceID) {
	s.ID = string(id)
}

var jsonMarshal = json.Marshal

// SignInstance the given instance cryptographically.
func (s *SpecterDB) SignInstance(pk db.PrivateKeyDB) (err error) {
	s.Signature = ""

	bytes, err := jsonMarshal(s)
	if err != nil {
		return
	}

	signature, _ := pk.Sign(bytes)
	s.Signature = EncodeKey(signature)
	return
}

// VerifyInstance that 'sig' is the signed hash of instance
func (s *SpecterDB) VerifyInstance(pk db.PublicKeyDB) (bool, error) {
	signature := s.Signature
	s.Signature = ""

	bytes, err := jsonMarshal(s)
	s.Signature = signature
	if err != nil {
		return false, err
	}

	valid := false
	decoded, err := DecodeKey(s.Signature)
	if err == nil {
		valid, err = pk.Verify(bytes, decoded)
	}

	return valid, err
}
