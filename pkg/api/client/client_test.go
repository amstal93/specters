package client

import (
	"context"
	"crypto/rand"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/stretchr/testify/assert"
	mockpb "gitlab.com/socialspecters.io/specters/internal/pkg/api/pb"
	"gitlab.com/socialspecters.io/specters/pkg/api/pb"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"testing"
)

func TestNewClient(t *testing.T) {
	tests := []struct {
		name         string
		target       string
		options      []grpc.DialOption
		expectingErr bool
	}{
		{
			name:   "All success no error",
			target: "bufnet",
			options: []grpc.DialOption{
				grpc.WithTransportCredentials(insecure.NewCredentials()),
			},
			expectingErr: false,
		},
		{
			name:         "Error creating client",
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			ctx := context.Background()
			_, err := NewClient(ctx, tc.target, tc.options...)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
		})
	}
}

func TestClient_Status(t *testing.T) {
	tests := []struct {
		name         string
		client       func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient
		expectingErr bool
	}{
		{
			name: "All success no error",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()
				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				mock.EXPECT().Status(ctx, &pb.StatusRequest{}).Return(&pb.StatusReply{}, nil)
				return mock
			},
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			ctx := context.Background()
			c, err := NewClient(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				tt.Fatalf("Failed creating new client: %v", err)
			}
			defer func(c *Client) {
				err = c.Close()
				if err != nil {
				}
			}(c)
			c.c = tc.client(tt, ctx)

			_, err = c.Status(ctx)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
		})
	}
}

func TestClient_Init(t *testing.T) {
	_, pubKey, err := crypto.GenerateEd25519Key(rand.Reader)
	if err != nil {
		t.Fatalf("Error creating root key")
	}
	pk, _ := specters.NewLibP2PPubKey(pubKey).MarshalBinary()

	tests := []struct {
		name             string
		client           func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient
		expectingErr     bool
		expectingErrCode codes.Code
	}{
		{
			name: "All success no error",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()
				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				mock.EXPECT().Init(ctx, &pb.InitRequest{
					RootPublicKey: pk,
				}).Return(&pb.InitReply{}, nil)
				return mock
			},
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			ctx := context.Background()
			c, err := NewClient(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				tt.Fatalf("Failed creating new client: %v", err)
			}
			defer func(c *Client) {
				err = c.Close()
				if err != nil {
				}
			}(c)
			c.c = tc.client(tt, ctx)

			_, err = c.Init(ctx, pk)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
		})
	}
}

func TestClient_Unseal(t *testing.T) {
	tests := []struct {
		name             string
		client           func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient
		expectingErr     bool
		expectingErrCode codes.Code
	}{
		{
			name: "All success no error",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()
				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				mock.EXPECT().Unseal(ctx, &pb.UnsealRequest{
					Key: []byte{0},
				}).Return(&pb.UnsealReply{}, nil)
				return mock
			},
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			ctx := context.Background()
			c, err := NewClient(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				tt.Fatalf("Failed creating new client: %v", err)
			}
			defer func(c *Client) {
				err = c.Close()
				if err != nil {
				}
			}(c)
			c.c = tc.client(tt, ctx)

			_, err = c.Unseal(ctx, []byte{0})
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
		})
	}
}

func TestClient_GetToken(t *testing.T) {
	decodedIdentity, err := specters.DecodeKey("CAESQPtFAFQMwikaJT5odV++QDsjJuGLeJbvpzbAMQwUKUgclfHXXbBAOy62afjjIM/rIyuVkZ7BG9TRTDp/3T1S7gI=")
	if err != nil {
		t.Fatal("Error decoding identity")
	}

	identity, err := specters.UnmarshalLibP2PIdentity(decodedIdentity)
	if err != nil {
		t.Fatal("Error unmarshalling identity")
	}

	signature, err := identity.Sign([]byte("challenge"))
	if err != nil {
		t.Fatal("Error signing challenge")
	}

	tests := []struct {
		name           string
		client         func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient
		identity       specters.Identity
		expectingToken string
		expectingErr   bool
	}{
		{
			name: "All success no error",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Key{
						Key: "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
					},
				})
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: []byte("challenge"),
					},
				}, nil)
				stream.EXPECT().Send(&pb.GetTokenRequest{
					Payload: &pb.GetTokenRequest_Signature{
						Signature: signature,
					},
				})
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Token{
						Token: "token",
					},
				}, nil)

				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "token",
			expectingErr:   false,
		},
		{
			name: "Error getting stream",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(nil, fmt.Errorf(""))

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   true,
		},
		{
			name: "Error closing send",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: []byte("challenge"),
					},
				}, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Token{
						Token: "token",
					},
				}, nil)
				stream.EXPECT().CloseSend().Return(fmt.Errorf(""))

				return mock
			},
			identity:       identity,
			expectingToken: "token",
			expectingErr:   true,
		},
		{
			name: "Error sending key token request",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any()).Return(fmt.Errorf(""))
				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   true,
		},
		{
			name: "Error io EOF sending key token request",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any()).Return(io.EOF)
				stream.EXPECT().RecvMsg(gomock.Any())
				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   false,
		},
		{
			name: "Error receiving empty challenge token reply",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{}, nil)
				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   true,
		},
		{
			name: "Error receiving challenge token reply",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(nil, fmt.Errorf(""))
				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   true,
		},
		{
			name: "Error sending signature token request",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: []byte("challenge"),
					},
				}, nil)
				stream.EXPECT().Send(gomock.Any()).Return(fmt.Errorf(""))
				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   true,
		},
		{
			name: "Error io EOF sending signature token request",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: []byte("challenge"),
					},
				}, nil)
				stream.EXPECT().Send(gomock.Any()).Return(io.EOF)
				stream.EXPECT().RecvMsg(gomock.Any())
				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   false,
		},
		{
			name: "Error receiving empty token reply",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: []byte("challenge"),
					},
				}, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{}, nil)
				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   true,
		},
		{
			name: "Error receiving token reply",
			client: func(t *testing.T, ctx context.Context) *mockpb.MockAPIClient {
				t.Helper()

				mock := mockpb.NewMockAPIClient(gomock.NewController(t))
				stream := mockpb.NewMockAPI_GetTokenClient(gomock.NewController(t))

				mock.EXPECT().GetToken(ctx).Return(stream, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(&pb.GetTokenReply{
					Payload: &pb.GetTokenReply_Challenge{
						Challenge: []byte("challenge"),
					},
				}, nil)
				stream.EXPECT().Send(gomock.Any())
				stream.EXPECT().Recv().Return(nil, fmt.Errorf(""))
				stream.EXPECT().CloseSend().Return(nil)

				return mock
			},
			identity:       identity,
			expectingToken: "",
			expectingErr:   true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			ctx := context.Background()
			c, err := NewClient(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				tt.Fatalf("Failed creating new client: %v", err)
			}
			defer func(c *Client) {
				err = c.Close()
				if err != nil {
				}
			}(c)
			c.c = tc.client(tt, ctx)

			token, err := c.GetToken(ctx, tc.identity)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
			assert.Equal(tt, tc.expectingToken, token)
		})
	}
}
