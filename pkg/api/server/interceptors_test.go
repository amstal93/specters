package server

import (
	"context"
	"crypto/rand"
	"github.com/golang/mock/gomock"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/stretchr/testify/assert"
	mockpb "gitlab.com/socialspecters.io/specters/internal/pkg/api/pb"
	mock_db "gitlab.com/socialspecters.io/specters/internal/pkg/db"
	"gitlab.com/socialspecters.io/specters/pkg/api/pb"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

func TestUnitaryStatusInterceptor(t *testing.T) {
	tests := []struct {
		name             string
		handler          func(t *testing.T) func(ctx context.Context, req interface{}) (interface{}, error)
		db               func(t *testing.T) *mock_db.MockDB
		expectingMsg     string
		expectingErr     bool
		expectingErrCode codes.Code
	}{
		{
			name: "All success no error",
			handler: func(t *testing.T) func(ctx context.Context, req interface{}) (interface{}, error) {
				return func(ctx context.Context, req interface{}) (interface{}, error) {
					return &pb.StatusReply{}, nil
				}
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			expectingMsg: "Specters runs correctly",
			expectingErr: false,
		},
		{
			name: "Specters is not initialized",
			handler: func(t *testing.T) func(ctx context.Context, req interface{}) (interface{}, error) {
				return func(ctx context.Context, req interface{}) (interface{}, error) {
					return &pb.StatusReply{}, nil
				}
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				return mock
			},
			expectingMsg:     "Specters is not initialized",
			expectingErr:     true,
			expectingErrCode: codes.FailedPrecondition,
		},
		{
			name: "Specters is sealed",
			handler: func(t *testing.T) func(ctx context.Context, req interface{}) (interface{}, error) {
				return func(ctx context.Context, req interface{}) (interface{}, error) {
					return &pb.StatusReply{}, nil
				}
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			expectingMsg:     "Specters is sealed",
			expectingErr:     true,
			expectingErrCode: codes.FailedPrecondition,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			key, _, err := crypto.GenerateRSAKeyPair(2048, rand.Reader)
			if err != nil {
				tt.Fatalf("Error generating server key: %v", err)
			}

			manager, err := specters.NewSpecters(context.Background(), "", "", tc.db(tt), specters.Config{Debug: false})
			if err != nil {
				tt.Fatalf("Error creating Specters manager")
			}

			if !tc.expectingErr {
				manager.SetIdentity(specters.NewLibP2PIdentity(key))
			}

			interceptor := UnaryStatusInterceptor(manager)

			info := &grpc.UnaryServerInfo{FullMethod: ""}
			_, err = interceptor(context.Background(), &pb.StatusRequest{}, info, tc.handler(tt))
			errExist := err != nil
			assert.Equal(tt, tc.expectingErr, errExist, err)

			if tc.expectingErr && tc.expectingErrCode != codes.OK {
				errStatus, _ := status.FromError(err)
				assert.Equal(tt, tc.expectingErrCode, errStatus.Code())
				if tc.expectingMsg != "" {
					assert.Equal(tt, tc.expectingMsg, errStatus.Message())
				}
			}
		})
	}
}

func TestStreamStatusInterceptor(t *testing.T) {
	tests := []struct {
		name             string
		client           func(t *testing.T) *mockpb.MockAPI_GetTokenServer
		streamHandler    func(t *testing.T) func(srv interface{}, stream grpc.ServerStream) error
		db               func(t *testing.T) *mock_db.MockDB
		expectingMsg     string
		expectingErr     bool
		expectingErrCode codes.Code
	}{
		{
			name: "All success no error",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()
				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				return mock
			},
			streamHandler: func(t *testing.T) func(srv interface{}, stream grpc.ServerStream) error {
				return func(srv interface{}, stream grpc.ServerStream) error {
					return nil
				}
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			expectingMsg: "Specters runs correctly",
			expectingErr: false,
		},
		{
			name: "Specters is not initialized",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()
				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				return mock
			},
			streamHandler: func(t *testing.T) func(srv interface{}, stream grpc.ServerStream) error {
				return func(srv interface{}, stream grpc.ServerStream) error {
					return nil
				}
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(false, nil)
				return mock
			},
			expectingMsg:     "Specters is not initialized",
			expectingErr:     true,
			expectingErrCode: codes.FailedPrecondition,
		},
		{
			name: "Specters is sealed",
			client: func(t *testing.T) *mockpb.MockAPI_GetTokenServer {
				t.Helper()
				mock := mockpb.NewMockAPI_GetTokenServer(gomock.NewController(t))
				return mock
			},
			streamHandler: func(t *testing.T) func(srv interface{}, stream grpc.ServerStream) error {
				return func(srv interface{}, stream grpc.ServerStream) error {
					return nil
				}
			},
			db: func(t *testing.T) *mock_db.MockDB {
				t.Helper()
				mock := mock_db.NewMockDB(gomock.NewController(t))
				mock.EXPECT().CreateSpecters(gomock.Any(), &specters.SpecterDB{}).Return(nil)
				mock.EXPECT().HasSpecters().Return(true, nil)
				return mock
			},
			expectingMsg:     "Specters is sealed",
			expectingErr:     true,
			expectingErrCode: codes.FailedPrecondition,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			key, _, err := crypto.GenerateRSAKeyPair(2048, rand.Reader)
			if err != nil {
				tt.Fatalf("Error generating server key: %v", err)
			}

			manager, err := specters.NewSpecters(context.Background(), "", "", tc.db(tt), specters.Config{Debug: false})
			if err != nil {
				tt.Fatalf("Error creating Specters manager")
			}

			if !tc.expectingErr {
				manager.SetIdentity(specters.NewLibP2PIdentity(key))
			}

			interceptor := StreamStatusInterceptor(manager)

			info := &grpc.StreamServerInfo{FullMethod: ""}
			err = interceptor(nil, tc.client(tt), info, tc.streamHandler(tt))
			errExist := err != nil
			assert.Equal(tt, tc.expectingErr, errExist, err)

			if tc.expectingErr && tc.expectingErrCode != codes.OK {
				errStatus, _ := status.FromError(err)
				assert.Equal(tt, tc.expectingErrCode, errStatus.Code())
				if tc.expectingMsg != "" {
					assert.Equal(tt, tc.expectingMsg, errStatus.Message())
				}
			}
		})
	}
}
